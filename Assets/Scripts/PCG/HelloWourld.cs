﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HelloWourld : MonoBehaviour
{
    public Text textObj;
    public string[] hellos = new string[4] { "Hello World",
                                             "Hola Mundo",
                                             "Bonjour Le Monde",
                                             "Hallo Welt"};

    void Start()
    {
        Random.InitState((int)System.DateTime.Now.Ticks); // Set the pseudo-random number seed (10ns/tick)
        InvokeRepeating("ChangeText", 0, 3);
    }
    
    void ChangeText()
    {
        int randIndex = Random.Range(0, hellos.Length);
        textObj.text = hellos[randIndex];
    }    
}
