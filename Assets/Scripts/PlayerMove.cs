﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class PlayerMove : MonoBehaviour
{
    // States
    const string S_WALK_RIGHT = "WalkingRight";
    const string S_WALK_LEFT = "WalkingLeft";
    const string S_WALK_UP = "WalkingUp";
    const string S_WALK_DOWN = "WalkingDown";

    // Parameters
    const string P_RIGHT = "right";
    const string P_LEFT = "left";
    const string P_UP = "up";
    const string P_DOWN = "down";

    public float speed = 2f;
    public bool up, down, left, right;
    public Animator playerAnimator;

	// Use this for initialization
	void Start ()
    {
        playerAnimator = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void FixedUpdate ()
    {
        AnimatePlayer();
	}

    void AnimatePlayer()
    {
        if(BeginMovingLeft())
        {
            playerAnimator.SetBool(P_LEFT, true);
            left = true;

            playerAnimator.SetBool(P_RIGHT, false);
            right = false;

            playerAnimator.SetBool(P_UP, false);
            up = false;

            playerAnimator.SetBool(P_DOWN, false);
            down = false;

            transform.Translate(Vector3.left * speed * Time.deltaTime);
        }

        if(BeginMovingRight())
        {
            playerAnimator.SetBool(P_LEFT, false);
            left = false;

            playerAnimator.SetBool(P_RIGHT, true);
            right = true;

            playerAnimator.SetBool(P_UP, false);
            up = false;

            playerAnimator.SetBool(P_DOWN, false);
            down = false;

            transform.Translate(Vector3.right * speed * Time.deltaTime);
        }

        if(BeginMovingUp())
        {
            playerAnimator.SetBool(P_LEFT, false);
            left = false;

            playerAnimator.SetBool(P_RIGHT, false);
            right = false;

            playerAnimator.SetBool(P_UP, true);
            up = true;

            playerAnimator.SetBool(P_DOWN, false);
            down = false;

            transform.Translate(Vector3.up * speed * Time.deltaTime);
        }

        if(BeginMovingDown())
        {
            playerAnimator.SetBool(P_LEFT, false);
            left = false;

            playerAnimator.SetBool(P_RIGHT, false);
            right = false;

            playerAnimator.SetBool(P_UP, false);
            up = false;

            playerAnimator.SetBool(P_DOWN, true);
            down = true;

            transform.Translate(Vector3.down * speed * Time.deltaTime);
        }

        if(MovingRight())
            transform.Translate(Vector3.right * speed * Time.deltaTime);
        if (MovingLeft())
            transform.Translate(Vector3.left * speed * Time.deltaTime);
        if (MovingUp())
            transform.Translate(Vector3.up * speed * Time.deltaTime);
        if (MovingDown())
            transform.Translate(Vector3.down * speed * Time.deltaTime);
    }

    bool BeginMovingRight()
    {
        return Input.GetKeyDown(KeyCode.RightArrow) || Input.GetKeyDown(KeyCode.D);
    }

    bool BeginMovingLeft()
    {
        return Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.A);
    }

    bool BeginMovingUp()
    {
        return Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.W);
    }

    bool BeginMovingDown()
    {
        return Input.GetKeyDown(KeyCode.DownArrow) || Input.GetKeyDown(KeyCode.S);
    }

    bool MovingRight()
    {
        return Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D);
    }

    bool MovingLeft()
    {
        return Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A);
    }

    bool MovingUp()
    {
        return Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.W);
    }

    bool MovingDown()
    {
        return Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.S);
    }
}
