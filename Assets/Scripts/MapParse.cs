﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml;

public class MapParse : MonoBehaviour
{
    public TextAsset levelInfo;

    GameObject map;

    XmlDocument xmlDoc;

    string imageName;
    Sprite[] mapSprites;

    public List<TileObject> tileList;
    public Dictionary<int, TileObject> tileDictionary;

    void Start ()
    {
        CreateMapObject();
        CreateXMLDoc();
        GetTileSheetImage();
        //ShowTileSheet();
        LoadMap();
	}
	
	
	void Update ()
    {
		
	}

    

    void CreateMapObject()
    {
        map = new GameObject();
        map.gameObject.name = "Map";
    }

    void CreateXMLDoc()
    {
        xmlDoc = new XmlDocument();
        xmlDoc.LoadXml(levelInfo.text);        
        
    }

    /// <summary>
    /// GetTilesheetImage stores the tilesheet into the sprites array
    /// </summary>
    void GetTileSheetImage()
    {
        imageName = xmlDoc.DocumentElement.ChildNodes[0].ChildNodes[0].Attributes["source"].InnerText;

        // Load the tiles into the array
        mapSprites = Resources.LoadAll<Sprite>(imageName);
    }


    void ShowTileSheet()
    {
        int width = 30,
            length = 35;

        int sprite = 0;

        for (int i = 0; i < length; i++)
        {
            for (int j = 0; j < width; j++)
            {
                GameObject tempObj = new GameObject();
                SpriteRenderer tempRender = tempObj.AddComponent<SpriteRenderer>();
                tempObj.transform.position = new Vector3(j, i, 0);
                tempRender.sprite = mapSprites[sprite];
                sprite++;
            }
        }        
    }
    void LoadMap()
    {        
        // Search noe names using their tag
        XmlNodeList layers = xmlDoc.DocumentElement.GetElementsByTagName("layer");
        // get layer width and height
        float tileWidth = float.Parse(xmlDoc.DocumentElement.ChildNodes[0].Attributes["tilewidth"].InnerText);
        float tileHeight = float.Parse(xmlDoc.DocumentElement.ChildNodes[0].Attributes["tileheight"].InnerText);

        XmlNodeList specialTiles = xmlDoc.DocumentElement.GetElementsByTagName("tile");
        tileDictionary = new Dictionary<int, TileObject>();

        foreach(XmlNode tile in specialTiles)
        {
            string type = tile.ChildNodes[0].ChildNodes[tile.ChildNodes[0].ChildNodes.Count - 1].Attributes["value"].InnerText;
            int tileID = int.Parse(tile.Attributes["id"].InnerText);
            switch(type)
            {
                case "door":

                    break;
            }
        }

        for (int layer = 0; layer < layers.Count; layer++)
        {
            GameObject parentObj = new GameObject();
            parentObj.name = layers[layer].Attributes["name"].InnerText;
            parentObj.transform.parent = map.transform;

            // Go through the Background Floor level
            float layerWidth = float.Parse(layers[layer].Attributes["width"].InnerText);
            float layerHeight = float.Parse(layers[layer].Attributes["height"].InnerText);

            // Select the data node
            XmlNode dataNode = layers[layer].SelectSingleNode("data");
            // First and last are empty
            // rows[0] && rows[rows.Length - 1]
            // Split apart the rows
            string[] rows = dataNode.InnerText.Split('\n');


            // Split the individual values in the rows from left to right
            for (int col = 1; col < layerHeight + 1; col++)
            {
                string[] values = rows[col].Split(',');
                for (int val = 0; val < layerWidth; val++)
                {
                    int tileVal = int.Parse(values[val]);

                    if (tileVal != 0)
                    {
                        GameObject tempObj = new GameObject();
                        SpriteRenderer tempRender = tempObj.AddComponent<SpriteRenderer>();
                        tempObj.transform.position = new Vector3(val, -col, 0);
                        tempObj.transform.parent = parentObj.transform;
                        tempObj.name = "Map Tile";
                        tempRender.sprite = mapSprites[tileVal - 1];
                        tempRender.sortingOrder = layer;

                        if (layers[layer].Attributes["name"].InnerText.Equals("Obstacle"))
                        {
                            BoxCollider2D tempCol = tempObj.AddComponent<BoxCollider2D>();
                            Rigidbody2D tempBody = tempObj.AddComponent<Rigidbody2D>();

                            tempBody.bodyType = RigidbodyType2D.Static;
                        }
                    }
                }
            }
        }
    }
}